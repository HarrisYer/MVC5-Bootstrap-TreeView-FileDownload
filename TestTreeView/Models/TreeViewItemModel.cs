﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTreeView.Models
{
    public class TreeViewItemModel
    {
        public string text { get; set; }
        public string nodeId { get; set; }
        public string parentId { get; set; }
        public string href { get; set; }
        public string color { get; set; }
        public string backColor { get; set; }
        public string icon { get; set; }
        public string path { get; set; }
        public string[] tags { get; set; }
        public bool selectable { get; set; }
        public List<TreeViewItemModel> nodes { get; set; }
    }//end class

    //public class TreeViewTagModel
    //{
    //    public TreeViewTagModel(int tag)
    //    {
    //        tags = tag;
    //    }

    //    public int tags { get; set; }
    //}
}//end namespace