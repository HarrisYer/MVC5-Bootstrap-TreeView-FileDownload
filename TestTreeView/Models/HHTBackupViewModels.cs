﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
namespace TestTreeView.Models
{
    public class HHTBackupViewModels
    {
        [Display(Name = "Paths")]
        public string FilePath { get; set; }
        public IEnumerable<SelectListItem> FilePathDDL { get; set; }
    }//end class
}//end namespace