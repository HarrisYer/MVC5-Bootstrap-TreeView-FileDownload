﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTreeView.Models
{
    public class CustomConfig
    {
        public string DisplayName { get; set; }
        public string DisplayValue { get; set; }
        public string KeyCategory { get; set; }
        public string KeyValue { get; set; }
        public string KeyStatus { get; set; }
    }//end class
}//end namespace