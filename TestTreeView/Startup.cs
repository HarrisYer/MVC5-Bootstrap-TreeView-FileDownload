﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestTreeView.Startup))]
namespace TestTreeView
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
