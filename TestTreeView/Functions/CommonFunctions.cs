﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
namespace TestTreeView.Functions
{
    public static class CommonFunctions
    {

        public static bool EqualDate(DateTime dt1, DateTime dt2)
        {
            if (dt1.CompareTo(dt2) == 0)
            {
                return true;
            }
            else { return false; }
        }//end function
        public static int intParse(object value)
        {
            if (value == null)
                return 0;
            int i;
            string v;
            v = Convert.ToString(value).Trim();
            if (v.Length == 0) { return 0; }
            i = (int)Math.Round(decimal.Parse(v), 0);
            return i;
        }
        public static long longParse(object value)
        {
            if (value == null)
                return 0;
            long i;
            string v;
            v = Convert.ToString(value).Trim();
            if (v.Length == 0) { return 0; }
            i = (long)Math.Round(decimal.Parse(v), 0);
            return i;
        }
        public static double doubleParse(object value)
        {
            if (value == null)
                return 0;
            double i;
            string v;
            v = Convert.ToString(value).Trim();
            if (v.Length == 0) { return 0; }
            i = double.Parse(v);
            return i;
        }
        public static decimal decimalParse(object value)
        {
            if (value == null)
                return 0;
            decimal i;
            string v;
            try
            {
                v = Convert.ToString(value).Trim();
                if (v.Length == 0) { return 0; }
                i = decimal.Parse(v);
                return i;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally { }
        }

        public static bool isEvenNumber(object value)
        {
            int a = intParse(value);
            if (a % 2 == 0)
                return true;
            return false;
        }

        // Function to Check for AlphaNumeric.
        public static bool IsAlphaNumeric(String strToCheck)
        {
            Regex objAlphaNumericPattern = new Regex("[^a-zA-Z0-9]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }
        // Function to test for Positive Integers. 
        public static bool IsNaturalNumber(String strNumber)
        {
            Regex objNotNaturalPattern = new Regex("[^0-9]");
            Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
            return !objNotNaturalPattern.IsMatch(strNumber) &&
            objNaturalPattern.IsMatch(strNumber);
        }
        // Function to test for Positive Integers with zero inclusive
        public static bool IsWholeNumber(String strNumber)
        {
            Regex objNotWholePattern = new Regex("[^0-9]");
            return !objNotWholePattern.IsMatch(strNumber);
        }
        // Function to Test for Integers both Positive & Negative
        public static bool IsInteger(String strNumber)
        {
            Regex objNotIntPattern = new Regex("[^0-9-]");
            Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
            return !objNotIntPattern.IsMatch(strNumber) && objIntPattern.IsMatch(strNumber);
        }
        // Function to Test for Positive Number both Integer & Real
        public static bool IsPositiveNumber(String strNumber)
        {
            Regex objNotPositivePattern = new Regex("[^0-9.]");
            Regex objPositivePattern = new Regex("^[.][0-9]+$|[0-9]*[.]*[0-9]+$");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            return !objNotPositivePattern.IsMatch(strNumber) &&
            objPositivePattern.IsMatch(strNumber) &&
            !objTwoDotPattern.IsMatch(strNumber);
        }
        // Function to test whether the string is valid number or not
        public static bool IsNumber(String strNumber)
        {
            Regex objNotNumberPattern = new Regex("[^0-9.-]");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
            String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
            String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
            Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");
            return !objNotNumberPattern.IsMatch(strNumber) &&
            !objTwoDotPattern.IsMatch(strNumber) &&
            !objTwoMinusPattern.IsMatch(strNumber) &&
            objNumberPattern.IsMatch(strNumber);
        }
        // Function To test for Alphabets.
        public static bool IsAlpha(String strToCheck)
        {
            Regex objAlphaPattern = new Regex("[^a-zA-Z]");
            return !objAlphaPattern.IsMatch(strToCheck);
        }
        public static string Left(string param, int length)
        {
            string result = "";
            if (param.Length < length)
            {
                result = param;
            }
            else
            {
                result = param.Substring(0, length);
            }//end if-else            
            return result;
        }
        public static string Right(string param, int length)
        {
            string result = "";
            if (param.Length < length)
            {
                result = param;
            }
            else
            {
                result = param.Substring(param.Length - length, length);
            }//end if-else  
            return result;
        }
        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }
        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }        

        /// <summary>
        /// Equal to VB.net IIF function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cond"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static T Iif<T>(bool cond, T left, T right)
        {
            return cond ? left : right;
        }

        /// <summary>
        /// Check collection is null and no records.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return true;
            }

            if (!enumerable.Any())
            {
                return true;
            }
            else
            {
                if (enumerable.FirstOrDefault().ToString() == "")
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsNumericType(this object o)
        {
            switch (Type.GetTypeCode(o.GetType()))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        public static string IsNullThenEmpty(string obj)
        {
            if (obj == null)
            { return ""; }
            return obj;
        }

        public static string IsNullThenZero(string obj)
        {
            if (obj == null)
            { return "0"; }
            return obj;
        }

        public static T IsNullThenNew<T>(this T obj)
        {
            if (obj == null)
            {
                return (T)Activator.CreateInstance(typeof(T));
            }
            else
            {
                return obj;
            }//end if-else
        }

        public static List<T> IsNullThenNew<T>(this IEnumerable<T> t)
        {
            if (!IsNullOrEmpty<T>(t))
            {
                return t.ToList();
            }
            else
            {
                Type genericListType = typeof(List<>);
                Type listType = genericListType.MakeGenericType(t.GetType());
                object listInstance = Activator.CreateInstance(listType);
                return (List<T>)listInstance;
            }//end if-else
        }

        public static IEnumerable<T> IsNullThenNewIEnum<T>(this IEnumerable<T> t)
        {
            if (!IsNullOrEmpty<T>(t))
            {
                return t.ToList();
            }
            else
            {
                Type genericListType = typeof(IEnumerable<>);
                Type listType = genericListType.MakeGenericType(t.GetType());
                object listInstance = Activator.CreateInstance(listType);
                return (IEnumerable<T>)listInstance;
            }//end if-else
        }

        public static bool IsAnEnumerable<T>(T t)
        {
            if (t == null)
            {
                return false;
            }//end if
            return (t is IEnumerable<T>);
        }

        public static List<SelectListItem> EmptySelectListItems()
        {
            return new List<SelectListItem>();
        }

        public static SelectList EmptySelectList()
        {
            return new SelectList(new List<SelectListItem>(), "Value", "Text");
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T GetObject<T>(Dictionary<string, string> dict)
        {
            Type type = typeof(T);
            var obj = Activator.CreateInstance(type);

            foreach (var kv in dict)
            {
                if (type.GetProperty(kv.Key) != null)
                    type.GetProperty(kv.Key).SetValue(obj, kv.Value);
            }
            return (T)obj;
        }

        public static bool IsDictionaryEmptyValues(Dictionary<string, string> dict)
        {
            bool istrue = false;

            istrue = dict.Select(u => u.Value != "").Count() > 0 ? false : true;
            return istrue;
        }

        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static void AddDictionary(Dictionary<string, string> dict, string Key, string Value)
        {
            Dictionary<string, string> a = new Dictionary<string, string>();

            if (dict.ContainsKey(Key))
            {
                return;
            }
            else
            {
                dict.Add(Key, Value);
            }

        }

    }//end class

   

    public static class CustomExpression //<T> : Expression<T> where T : class
    {
        public static IEnumerable<T> ToEnumerable<T>(this T input)
        {
            yield return input;
        }
        public static Expression<Func<T, bool>> CreatePropertyAccessor<T>(string target, string value)
        {
            var param = Expression.Parameter(typeof(T));
            MemberExpression fieldExp = Expression.PropertyOrField(param, target);

            MemberExpression member = Expression.Property(param, target);
            ConstantExpression value1 = Expression.Constant(value);
            BinaryExpression assignExp = Expression.Equal(member, value1);

            Expression<Func<T, bool>> where = Expression.Lambda<Func<T, bool>>(assignExp, param);
            return where;
        }

        public static Expression<Func<T, bool>> AndAlso<T>(Expression<Func<T, bool>> e1, Expression<Func<T, bool>> e2)
        {
            var lambda1 = Expression.Lambda<Func<T, bool>>(Expression.AndAlso(
            new SwapVisitor(e1.Parameters[0], e2.Parameters[0]).Visit(e1.Body),
            e2.Body), e2.Parameters);
            return lambda1;
        }

        public static IQueryable<T> IQueryable<T>(this IQueryable<T> source, string PropertyName, string TagName, string order)
        {
            var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
            System.Linq.Expressions.Expression parent = param1;
            parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
            Expression conversion = Expression.Convert(parent, typeof(object));
            //var prop = Expression.Property(param1, PropertyName);

            ParameterExpression param = Expression.Parameter(typeof(T), TagName);
            Expression<Func<T, object>> sortExpression1 = Expression.Lambda<Func<T, object>>(
                                                                            Expression.Convert(
                                                                                Expression.Property(param, PropertyName),
                                                                                typeof(object)),
                                                                            param);

            return IQueryableNullable<T>(source, sortExpression1, order);


            ////if (!CustomExpression.isNullable<T>(PropertyName, "tags"))
            ////{

            ////    switch (Type.GetTypeCode(parent.Type))
            ////    {
            ////        //case TypeCode.Byte:
            ////        //case TypeCode.SByte:
            ////        // case TypeCode.UInt16:
            ////        //case TypeCode.UInt32:
            ////        // case TypeCode.UInt64:
            ////        case TypeCode.Int16:
            ////            System.Linq.Expressions.Expression<Func<T, Int16>> sortExpressionInt16 = CustomExpression.LambaExpressionInt16<T>(PropertyName, "tags");
            ////            return IQueryableInt16<T>(source, sortExpressionInt16, order);
            ////        case TypeCode.Int32:
            ////            //System.Linq.Expressions.Expression<Func<T, Int32>> sortExpressionInt32 = CustomExpression.LambaExpressionInt32<T>(PropertyName, "tags");
            ////            //return IQueryableInt32<T>(source, sortExpressionInt32, order);
            ////            return IQueryableNullable<T>(source, sortExpression1, order);
                        
            ////        //case TypeCode.Int64:
            ////        //    System.Linq.Expressions.Expression<Func<T, Int64>> sortExpressionInt64 = CustomExpression.LambaExpression<T>(PropertyName, "tags");
            ////        //    return IQueryable<T>(source, sortExpressionInt64, order);
            ////        case TypeCode.Decimal:
            ////            System.Linq.Expressions.Expression<Func<T, Decimal>> sortExpressionDecimal = CustomExpression.LambaExpressionDecimal<T>(PropertyName, "tags");
            ////            return IQueryableDecimal<T>(source, sortExpressionDecimal, order);
            ////        case TypeCode.DateTime:
            ////            System.Linq.Expressions.Expression<Func<T, DateTime>> sortExpressionDateTime = CustomExpression.LambaExpressionDateTime<T>(PropertyName, "tags");
            ////            return IQueryableDateTime<T>(source, sortExpressionDateTime, order);
            ////        //case TypeCode.Single:
            ////        default:
            ////            System.Linq.Expressions.Expression<Func<T, object>> sortExpression = CustomExpression.LambaExpression<T>(PropertyName, "tags");
            ////            return IQueryable<T>(source, sortExpression, order);
            ////    }//end switch
            ////}
            ////else
            ////{
            ////    return IQueryableNullable<T>(source, sortExpression1, order);
            ////    //switch (Type.GetTypeCode(Nullable.GetUnderlyingType(parent.Type)))
            ////    //{
            ////    //    case TypeCode.Int32:
            ////    //       // System.Linq.Expressions.Expression<Func<T, Int32?>> sortExpression = Expression.Lambda<Func<T, Int32?>>(conversion, param1);
            ////    //       // return IQueryableNullableInt32<T>(source, sortExpression1, order);
            ////    //        return IQueryableNullable<T>(source, sortExpression1, order);
            ////    //    case TypeCode.DateTime:
            ////    //        //Expression conversiondt = Expression.Convert(parent, typeof(DateTime?));
            ////    //        //ParameterExpression param = Expression.Parameter(typeof(T), TagName);
            ////    //        //Expression<Func<T, object>> sortExpressionDatetime = Expression.Lambda<Func<T, object>>(
            ////    //        //                                                    Expression.Convert(
            ////    //        //                                                        Expression.Property(param, PropertyName),
            ////    //        //                                                        typeof(object)),
            ////    //        //                                                    param);
            ////    //        //System.Linq.Expressions.Expression<Func<T, DateTime?>> sortExpressionDatetime = Expression.Lambda<Func<T, DateTime?>>(conversion, param1);
            ////    //        return IQueryableNullable<T>(source, sortExpression1, order);
            ////    //    default:
            ////    //        //System.Linq.Expressions.Expression<Func<T, Int32?>> sortExpressionDefault = Expression.Lambda<Func<T, Int32?>>(conversion, param1);
            ////    //        return IQueryableNullable<T>(source, sortExpression1, order);
            ////    //}//end switch
            ////}//end if-else

        }

        #region IQueryable<T>

        public static IQueryable<T> IQueryable<T>(this IQueryable<T> source,System.Linq.Expressions.Expression<Func<T, object>> sortExpression, string order)
        {
            if (order == CommonSetting.Ordering.Decending)
            {
                return source
                       .OrderByDescending(sortExpression);
            }
            else
            {
                return source
                       .OrderBy(sortExpression);
            }//end if-else
        }

        //public static IQueryable<T> IQueryableInt16<T>(this IQueryable<T> source, System.Linq.Expressions.Expression<Func<T, Int16>> sortExpression, string order)
        //{
        //    if (order == CommonSetting.Ordering.Decending)
        //    {
        //        return source
        //               .OrderByDescending(sortExpression);
        //    }
        //    else
        //    {
        //        return source
        //               .OrderBy(sortExpression);
        //    }//end if-else
        //}

        //public static IQueryable<T> IQueryableInt32<T>(this IQueryable<T> source, System.Linq.Expressions.Expression<Func<T, Int32>> sortExpression, string order)
        //{
        //    if (order == CommonSetting.Ordering.Decending)
        //    {
        //        return source
        //               .OrderByDescending(sortExpression);
        //    }
        //    else
        //    {
        //        return source
        //               .OrderBy(sortExpression);
        //    }//end if-else
        //}
        //public static IQueryable<T> IQueryableDecimal<T>(this IQueryable<T> source, System.Linq.Expressions.Expression<Func<T, Decimal>> sortExpression, string order)
        //{
        //    if (order == CommonSetting.Ordering.Decending)
        //    {
        //        return source
        //               .OrderByDescending(sortExpression);
        //    }
        //    else
        //    {
        //        return source
        //               .OrderBy(sortExpression);
        //    }//end if-else
        //}

        //public static IQueryable<T> IQueryableDateTime<T>(this IQueryable<T> source, System.Linq.Expressions.Expression<Func<T, DateTime>> sortExpression, string order)
        //{
        //    if (order == CommonSetting.Ordering.Decending)
        //    {
        //        return source
        //               .OrderByDescending(sortExpression);
        //    }
        //    else
        //    {
        //        return source
        //               .OrderBy(sortExpression);
        //    }//end if-else
        //}

        //public static IQueryable<T> IQueryableNullableInt32<T>(this IEnumerable<T> source, System.Linq.Expressions.Expression<Func<T, Int32?>> sortExpression, string order)
        //{


        //    if (order == CommonSetting.Ordering.Decending)
        //    {

        //        return source.ToList()
        //               .OrderByDescending(sortExpression.Compile()).AsQueryable();
        //    }
        //    else
        //    {
        //        return source.ToList().OrderBy(sortExpression.Compile()).AsQueryable();
        //        //.OrderBy(sortExpression).ToList();
        //    }//end if-else
        //}

        public static IQueryable<T> IQueryableNullable<T>(this IEnumerable<T> source, System.Linq.Expressions.Expression<Func<T, object>> sortExpression, string order)
        {


            if (order == CommonSetting.Ordering.Decending)
            {

                return source.ToList()
                       .OrderByDescending(sortExpression.Compile()).AsQueryable();
            }
            else
            {
                return source.ToList().OrderBy(sortExpression.Compile()).AsQueryable();
                //.OrderBy(sortExpression).ToList();
            }//end if-else
        }

        //public static IQueryable<T> IQueryableNullableDatetime<T>(this IEnumerable<T> source, System.Linq.Expressions.Expression<Func<T, object>> sortExpression, string order)
        //{


        //    if (order == CommonSetting.Ordering.Decending)
        //    {

        //        return source.ToList()
        //               .OrderByDescending(sortExpression.Compile()).AsQueryable();
        //    }
        //    else
        //    {
        //        return source.ToList().OrderBy(sortExpression.Compile()).AsQueryable();
        //        //.OrderBy(sortExpression).ToList();
        //    }//end if-else
        //}


        public static Expression<Func<T, object>> LambaExpressionDefault<T>(string PropertyName, string TagName)
        {
            ParameterExpression param = Expression.Parameter(typeof(T), TagName);
            Expression<Func<T, object>> sortExpression1 = Expression.Lambda<Func<T, object>>(
                                                                            Expression.Convert(
                                                                                Expression.Property(param, PropertyName),
                                                                                typeof(object)),
                                                                            param);
            return sortExpression1;
        }

        public static Expression<Func<T, object>> LambaExpression<T>(string PropertyName, string TagName)
        {
            var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
            System.Linq.Expressions.Expression parent = param1;
            parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
            //Func<T, Object> func;
            //try
            //{
            //    Expression conversion = Expression.Convert(parent, typeof(object));
            //    func = Expression.Lambda<Func<T, Object>>(conversion, param1).Compile();
            //    return Expression.Lambda<Func<T, object>>(conversion, param1);
            //}
            //catch (Exception ex)
            //{
            //    var a = ex.Message;
            //}
            //finally { }



            var sortExpression = System.Linq.Expressions.Expression.Lambda<Func<T, object>>(parent, param1);
            return sortExpression;
        }

        //public static Expression<Func<T, Int16>> LambaExpressionInt16<T>(string PropertyName, string TagName)
        //{
        //    var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
        //    System.Linq.Expressions.Expression parent = param1;
        //    parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
        //    var sortExpression = System.Linq.Expressions.Expression.Lambda<Func<T, Int16>>(parent, param1);
        //    return sortExpression;
        //}

        //public static Expression<Func<T, Int32>> LambaExpressionInt32<T>(string PropertyName, string TagName)
        //{
        //    var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
        //    System.Linq.Expressions.Expression parent = param1;
        //    parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
        //    //Func<T, Object> func;
        //    //try
        //    //{
        //    //    Expression conversion = Expression.Convert(parent, typeof(object));
        //    //    func = Expression.Lambda<Func<T, Object>>(conversion, param1).Compile();
        //    //    return Expression.Lambda<Func<T, object>>(conversion, param1);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    var a = ex.Message;
        //    //}
        //    //finally { }



        //    var sortExpression = System.Linq.Expressions.Expression.Lambda<Func<T, Int32>>(parent, param1);
        //    return sortExpression;
        //}

        //public static Expression<Func<T, Decimal>> LambaExpressionDecimal<T>(string PropertyName, string TagName)
        //{
        //    var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
        //    System.Linq.Expressions.Expression parent = param1;
        //    parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
        //    var sortExpression = System.Linq.Expressions.Expression.Lambda<Func<T, Decimal>>(parent, param1);
        //    return sortExpression;
        //}

        //public static Expression<Func<T, DateTime>> LambaExpressionDateTime<T>(string PropertyName, string TagName)
        //{
        //    var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
        //    System.Linq.Expressions.Expression parent = param1;
        //    parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
        //    var sortExpression = System.Linq.Expressions.Expression.Lambda<Func<T, DateTime>>(parent, param1);
        //    return sortExpression;
        //}

        #endregion

        public static System.Type propertyType<T>(string PropertyName, string TagName)
        {
            var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
            var prop = Expression.Property(param1, PropertyName);

            return prop.Type;


        }

        public static bool isNumericType<T>(string PropertyName, string TagName)
        {
            var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
            var prop = Expression.Property(param1, PropertyName);

            return prop.IsNumericType();

           
        }

        public static bool isNullable<T>(string PropertyName, string TagName)
        {
            var param1 = System.Linq.Expressions.Expression.Parameter(typeof(T), TagName);
            var prop = Expression.Property(param1, PropertyName);

            if (prop.Type.IsGenericType && prop.Type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                return true;
            }
            return false;
            
           // System.Linq.Expressions.Expression parent = param1;
           // parent = System.Linq.Expressions.Expression.Property(parent, PropertyName);
           // var sortExpression = System.Linq.Expressions.Expression.Lambda(parent, param1);
            //return exp;
        }
    }

    public class SwapVisitor : ExpressionVisitor
    {
        private readonly Expression from, to;
        public SwapVisitor(Expression from, Expression to)
        {
            this.from = from;
            this.to = to;
        }
        public override Expression Visit(Expression node)
        {
            return node == from ? to : base.Visit(node);
        }
    }

    public static class Extensions
    {
        public static IQueryable<TSource> WhereIf<TSource>(
        this IQueryable<TSource> source, bool condition,
        Expression<Func<TSource, bool>> predicate)
            {
                if (condition)
                    return source.Where(predicate);
                else
                    return source;
            }
        

        public static IQueryable<TResult>LeftJoin<TSource, TInner, TKey, TResult>(this IQueryable<TSource> source,
                                                         IQueryable<TInner> inner, 
                                                         Func<TSource, TKey> pk, 
                                                         Func<TInner, TKey> fk, 
                                                         Func<TSource, TInner, TResult> result)
        {
            IQueryable<TResult> _result;// =  Queryable.Empty<TResult>();
 
            _result = from s in source
                      join i in inner
                      on pk(s) equals fk(i) into joinData
                      from left in joinData.DefaultIfEmpty()
                      select result(s, left);
 
            return _result;
        }

        public static IEnumerable<TSource> WhereIf<TSource>(
       this IEnumerable<TSource> source, bool condition,
       Expression<Func<TSource, bool>> predicate)
        {
            if (condition)
                return source.Where(predicate.Compile()).ToList();
            else
                return source;
        }
    }//end class
}//end namespace