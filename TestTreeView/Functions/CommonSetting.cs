﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTreeView.Functions
{
    public class CommonSetting
    {
        public const string DateFormatFromDatePicker = "dd/M/yyyy";
        /// <summary>
        /// dd/MM/yy
        /// </summary>
        public const string DateFormatCalendar = "dd/MM/yy";        
        /// <summary>
        /// dd/MM/yyyy
        /// </summary>
        public const string DateFormatDDMMYY = "dd/MM/yyyy";
        public const string DateFormatDDMMMYY = "dd-MMM-yyyy";
        public const string DefaultDate = "1900-01-01";
        public const int DefaultDateYear = 1900;
        public const int DefaultDateMonth = 1;
        public const int DefaultDateDay = 1;
        public const string DateFormatYYMMDD = "yyy/MMM/dd";
        public const string DateFormatDDMMYYHHNNSS = "dd/MM/yyyy hh:mm:ss";
        public const string DateFormatYYMMDDHHNNSS = "yyy/MMM/dd hh:mm:ss";
        public const string DropDownListDashes = "-";

        public const string FillOutTheBlank = "Please fill out this field.";
        public const string ValidationSumarryHeader = "Please clean the following errors and try again"; 
        public const string SuccessModifyRecords = "Success modify records!";
        public const string PleaseContactAdmin = "Errors, please contact admin!";
        public const string ApproveError = "Approve Errors, please contact admin!";
        public const string ApproveSuccess = "Approve Successful!";
        public const string ApproveFail = "Fail Update record!";
        public const string SuccessUpdateRecord = "Success Update record!";
        public const string SelectOneRecord = "Please select at least 1 record!";
        public const string FillInAllSearch = "Please fill in all search Criteria!";

        public const string RequestTypePOST = "POST";

        public struct Ordering
        {
            public const string Accending = "asc";
            public const string Decending = "desc";
            
        }

        public struct Status
        {
            public const string Active = "1";
            public const string UnActive = "0";
            public const string Pending = "3";
        }

        public struct TempDataKeys
        {
            public const string ListPageId = "ListPageId";
            //public const string ListDeactiveURL = "ListDeactiveURL";
            public const string IsPassbackError = "IsPassbackError";
            public const string PassbackErrorMsg = "PassbackErrorMsg";
        }


    }//end class
}//end namespace