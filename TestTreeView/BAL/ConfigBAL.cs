﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using System.Web.Mvc;
using TestTreeView.Models;
using TestTreeView.Functions;
namespace TestTreeView.BAL
{
    public interface IConfigBAL : IDisposable
    {
        List<SelectListItem> GetSelectedItemsByCategory(string KeyCategory);
        string GetConfigKeyValueByCategoryDisplayValue(string KeyCategory, string DisplayValue);
    }

    public class ConfigBAL : IConfigBAL
    {
        readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<CustomConfig> LoadConfigJson()
        {
            List<CustomConfig> items = new List<CustomConfig>(); 
            string FilePath=HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["CustomConfigFilePath"]);
            using (StreamReader r = new StreamReader(FilePath))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<CustomConfig>>(json);
            }
            return items;
        }

        public List<CustomConfig> GetConfigByCategory(string KeyCategory)
        {
            List<CustomConfig> items = new List<CustomConfig>(); 
            try
            {
                items = LoadConfigJson();
                items = items.Where(x => x.KeyCategory == KeyCategory).ToList();   
            }
            catch (Exception ex)
            {
                Logger.Error("Error", ex);
            }
            finally { }
            return items;
        }

        public string GetConfigKeyValueByCategoryDisplayValue(string KeyCategory, string DisplayValue)
        {
            List<CustomConfig> items = new List<CustomConfig>();
            try
            {
                items = LoadConfigJson();
                items = items.Where(x => x.KeyCategory == KeyCategory && x.DisplayValue ==DisplayValue && x.KeyStatus =="1").ToList();
                if (!items.IsNullOrEmpty())
                {
                    return items.FirstOrDefault().KeyValue;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error", ex);
            }
            finally { }
            return "";
        }

        public List<SelectListItem> GetSelectedItemsByCategory(string KeyCategory)
        {
            List<CustomConfig> items = new List<CustomConfig>();
            List<SelectListItem> infos = new List<SelectListItem>();
            try
            {
               infos= GetConfigByCategory(KeyCategory)
                .Where(r=>r.KeyStatus=="1") 
                .Select(r => new SelectListItem()
                   {
                       Text = r.DisplayName ,
                       Value = r.DisplayValue 
                   }).OrderBy(r=>r.Value).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error", ex);
            }
            finally { }
            return infos;
        }

        public void Dispose()
        {
           
        }
    }//end class
}//end namespace