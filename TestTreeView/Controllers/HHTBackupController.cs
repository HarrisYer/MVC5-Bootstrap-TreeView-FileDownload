﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using System.Web;
using TestTreeView.Models;
using TestTreeView.BAL;
using TestTreeView.Functions;
using Newtonsoft.Json;
namespace TestTreeView.Controllers
{
    public class HHTBackupController : Controller
    {
        public ActionResult List()
        {
            HHTBackupViewModels model = new HHTBackupViewModels(); 
            using (IConfigBAL configBAL = new ConfigBAL())
            {
                model.FilePathDDL = configBAL.GetSelectedItemsByCategory("UploadPath");
            }
            return View(model);
        }

        public void ProcessDirectory(int ParentId,int SelectSize,string PathValue,string PrefixPath, string targetDirectory, List<TreeViewItemModel> tvtmList)
        {
            int NodeIdCount = ParentId + 1;
            int fileCount = 0;
            int dirCount = 0;
            try
            {
                TreeViewItemModel tvimDirtarget = new TreeViewItemModel();
                var dirtarget = new DirectoryInfo(targetDirectory);
                tvimDirtarget.text = dirtarget.Name;
                tvimDirtarget.nodeId = NodeIdCount.ToString();
                tvimDirtarget.parentId = ParentId.ToString();
                tvimDirtarget.selectable = false;
                tvimDirtarget.color = "#000000";
                tvimDirtarget.icon = "glyphicon";

                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                dirCount = subdirectoryEntries.Count();
                foreach (string subdirectory in subdirectoryEntries)
                {
                    if (tvimDirtarget.nodes == null)
                    {
                        tvimDirtarget.nodes = new List<TreeViewItemModel>();
                    }//end if
                    ProcessDirectory(NodeIdCount, SelectSize, PathValue, PrefixPath, subdirectory, tvimDirtarget.nodes);

                }//end foreach



                FileInfo[] files = dirtarget.GetFiles();
                if (tvimDirtarget.nodes == null)
                {
                    tvimDirtarget.nodes = new List<TreeViewItemModel>();
                }//end if

                files = files.OrderByDescending(m=>m.CreationTime).ToArray();

                for (int i = 0; i < files.Length; i++)
                {
                    if (fileCount >= SelectSize)
                    {
                        break;
                    }//end if

                    FileInfo file = files[i];
                    TreeViewItemModel tvim = new TreeViewItemModel();
                    tvim.text = file.Name;
                    tvim.nodeId = NodeIdCount.ToString();
                    tvim.parentId = ParentId.ToString();
                    tvim.selectable = true;
                    tvim.path = file.FullName.Replace(PrefixPath, "");

                    tvim.href = Url.Action("DownloadAttachment", new
                                    {
                                        path = file.FullName.Replace(PrefixPath, ""),
                                        PathValue = PathValue
                                    });
                    tvim.icon = "glyphicon glyphicon-download";
                    tvim.tags = new string[] { "0" };
                    tvimDirtarget.nodes.Add(tvim);
                    NodeIdCount++;
                    fileCount = fileCount + 1;
                }//end for

                tvimDirtarget.tags = new string[] { (fileCount + dirCount).ToString() };
                tvtmList.Add(tvimDirtarget);

            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally { }



        }

        public JsonResult PopulateFileList(string selectSize,string PathValue)
        {
            List<TreeViewItemModel> tvtmList = new List<TreeViewItemModel>();

            string filePath = "";
            using (IConfigBAL configBAL = new ConfigBAL())
            {
                filePath = configBAL.GetConfigKeyValueByCategoryDisplayValue("UploadPath", PathValue);
            } //end using 
          

            int NodeIdCount = 100;
            int intSize =CommonFunctions.Iif(CommonFunctions.intParse(selectSize)==0,10000,CommonFunctions.intParse(selectSize));
            ProcessDirectory(NodeIdCount, intSize, PathValue, filePath, filePath, tvtmList);

            return Json(tvtmList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DownloadAttachment(string path, string PathValue)
        {

            string filePath = "";
            using (IConfigBAL configBAL = new ConfigBAL())
            {
                filePath = configBAL.GetConfigKeyValueByCategoryDisplayValue("UploadPath", PathValue) + path;
            }  
       
            try
            {
                if (System.IO.File.Exists(filePath))
                {
                    string filename = Path.GetFileName(filePath);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
                }//end if


            }
            catch (Exception ex)
            {
                //var a = ex.Message;
            }
            finally { }

            return Json(new { Success = false }, JsonRequestBehavior.AllowGet);

        }


    }//end class
}//end namespace